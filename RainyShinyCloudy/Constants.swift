//
//  Constants.swift
//  RainyShinyCloudy
//
//  Created by Yaroslav Zhurbilo on 18.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation

typealias DownloadComplete = () -> ()

//let BASE_URL  = "http://samples.openweathermap.org/data/2.5/weather?"
//let LATITUDE  = "lat="
//let LONGITUDE = "lon="
//let APP_ID    = "appid="
//let API_KEY   = "b1b15e88fa797225412429c1c50c122a1"
