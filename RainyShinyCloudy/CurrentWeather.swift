//
//  CurrentWeather.swift
//  RainyShinyCloudy
//
//  Created by Yaroslav Zhurbilo on 18.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    private var _cityName: String!
    private var _weatherType: String!
    private var _currentTemp: Double!
    private var _date: String!
    
    var weatherApi: WeatherAPI!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    var currentTemp: Double {
        if _currentTemp == nil {
            _currentTemp = 0
        }
        return _currentTemp
    }
    var date: String {
        if _date == nil {
            _date = ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)"
        return _date
    }
    
    func downloadWeatherDetails(completed: @escaping DownloadComplete) {
        weatherApi = WeatherAPI(location: "Kiev")
        let requestedURL = URL(string: weatherApi.requestedURL)
        if let requestedURL = requestedURL {
            Alamofire.request(requestedURL).responseJSON { response in
                let result = response.result
                
                if let dict = result.value as? Dictionary<String, AnyObject> {
                    if let name = dict["name"] as? String {
                        self._cityName = name.capitalized
                    }
                    if let weather = dict["weather"] as? [Dictionary<String, AnyObject>] {
                        if let type = weather[0]["main"] as? String {
                            self._weatherType = type.capitalized
                        }
                    }
                    if let temp = dict["main"]?["temp"] as? Double {
                        let kelvinToCelsius = temp - 273.15
                        self._currentTemp = Double(round(kelvinToCelsius))
                    }
                }
                completed()
                // it is important to put completed in the block!!!
            }
        }
    }
    
}














