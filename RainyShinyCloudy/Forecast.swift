//
//  Forecast.swift
//  RainyShinyCloudy
//
//  Created by Yaroslav Zhurbilo on 18.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import Alamofire

class Forecast {
    private var _date: String!
    private var _weatherType: String!
    private var _maxTemp: Double!
    private var _minTemp: Double!
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    var maxTemp: Double {
        if _maxTemp == nil {
            _maxTemp = 0
        }
        return _maxTemp
    }
    var minTemp: Double {
        if _minTemp == nil {
            _minTemp = 0
        }
        return _minTemp
    }
    
    init() {
        
    }
    
    init(weatherDict: Dictionary<String, AnyObject>) {
        if let weatherType = (weatherDict["weather"] as? [Dictionary<String, AnyObject>])?[0]["main"] as? String {
            self._weatherType = weatherType
        }
        if let maxTemp = (weatherDict["temp"] as? Dictionary<String, AnyObject>)?["max"] as? Double {
            let kelvinToCelsius = maxTemp - 273.15
            self._maxTemp = Double(round(kelvinToCelsius))
        }
        if let minTemp = (weatherDict["temp"] as? Dictionary<String, AnyObject>)?["min"] as? Double {
            let kelvinToCelsius = minTemp - 273.15
            self._minTemp = Double(round(kelvinToCelsius))
        }
        if let dt = weatherDict["dt"] as? Int {
            let unixConvertedDate = Date(timeIntervalSince1970: TimeInterval(dt))
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "EEEE"
            dateFormatter.timeStyle = .none
            self._date = unixConvertedDate.dayOfTheWeek()
        }
    }
    
}











