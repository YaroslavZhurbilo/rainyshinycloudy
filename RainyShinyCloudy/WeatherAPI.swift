//
//  WeatherAPI.swift
//  RainyShinyCloudy
//
//  Created by Yaroslav Zhurbilo on 18.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation


class WeatherAPI {
    
    private let API_KEY   = "ae8a626047cfc7ea6ed0cf698c308815"
    
    private var BASE_URL: String {
        var url = "http://api.openweathermap.org/data/2.5/"
        switch requestType {
        case .CurrentWeather:
            url += "weather?"
            return url
        case .Forecast16Days:
            url += "forecast/daily?"
            return url
        }
    }
    
    private var appIdURL: String {
        return "&appid=" + API_KEY
    }
    private var latURL: String {
        return "lat=" + "\(latitude!)"
    }
    private var lonURL: String {
        return "&lon=" + "\(longitude!)"
    }
    private var cntURL: String {
        return "&cnt=" + "\(count)"
    }
    private var count: Int {
        switch requestType {
        case .CurrentWeather: return 1
        case .Forecast16Days: return 16
        }
    }
    
    var longitude: Double!
    var latitude:  Double!
    var defaultLocation = "Kiev"
    var requestType: RequestType
    
    var requestedURL: String {
        
        var url = BASE_URL
        switch requestType {
        case .CurrentWeather: url = url + latURL + lonURL
        case .Forecast16Days: url = url + latURL + lonURL + cntURL
        }
        
        return (url + appIdURL)
    }
    
    init() {
        
        if let longitude = Location.sharedInstance.longitude, let latitude = Location.sharedInstance.latitude {
            self.longitude = longitude
            self.latitude  = latitude
        } else if let city = locations[defaultLocation] {
            self.longitude = city["lon"]
            self.latitude  = city["lat"]
        } else {
            self.longitude = 30.52
            self.latitude  = 50.43
        }
        self.requestType = .CurrentWeather
        
    }
    
    init(location: String, requestType: RequestType = .CurrentWeather) {
        
        if let longitude = Location.sharedInstance.longitude, let latitude = Location.sharedInstance.latitude {
            self.longitude = longitude
            self.latitude  = latitude
        } else if let city = locations[location.capitalized] {
            self.longitude = city["lon"]
            self.latitude  = city["lat"]
        } else {
            self.longitude = 30.52
            self.latitude  = 50.43
        }
        self.requestType = requestType
        
    }
    
    init(longitude: Double, latitude: Double, requestType: RequestType = .CurrentWeather) {
        
        if let longitude = Location.sharedInstance.longitude, let latitude = Location.sharedInstance.latitude {
            self.longitude = longitude
            self.latitude  = latitude
        } else {
            self.longitude = longitude
            self.latitude  = latitude
        }
        self.requestType = requestType
    }
    
    private let locations: [String: [String:Double]] = [
        "Kiev" : ["lon":30.52, "lat":50.43],
        "Kyiv" : ["lon":30.52, "lat":50.43],
        "Kyyiv" : ["lon":30.52, "lat":50.43],
        "London" : ["lon":-0.13, "lat":51.51],
        "Behbahan" : ["lon":50.24, "lat":30.6]
    ]
    
    enum RequestType {
        case CurrentWeather
        case Forecast16Days
    }
}
