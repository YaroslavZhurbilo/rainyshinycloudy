//
//  WeatherCell.swift
//  RainyShinyCloudy
//
//  Created by Yaroslav Zhurbilo on 18.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var dayOfTheWeek: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    
    func updateCell(forecast: Forecast) {
        weatherImage.image = UIImage(named: forecast.weatherType)
        weatherType.text = forecast.weatherType
        maxTemp.text = "\(forecast.maxTemp)"
        minTemp.text = "\(forecast.minTemp)"
        dayOfTheWeek.text = forecast.date
    }

}
