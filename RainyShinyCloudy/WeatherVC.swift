//
//  WeatherVC.swift
//  RainyShinyCloudy
//
//  Created by Yaroslav Zhurbilo on 13.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class WeatherVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var weaterLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    var currentWeather: CurrentWeather!
    var forecast: Forecast!
    var forecasts = [Forecast]()
    var weatherApi: WeatherAPI!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        locationAuthStatus()
        
        currentWeather = CurrentWeather()
        forecast = Forecast()
        
        currentWeather.downloadWeatherDetails {
            self.downloadForecastData {
                self.updateMainUI()
                self.tableView.reloadData()
            }
        }
    }
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            if let location = locationManager.location {
                currentLocation = location
                Location.sharedInstance.latitude = currentLocation.coordinate.latitude
                Location.sharedInstance.longitude = currentLocation.coordinate.longitude
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
            locationAuthStatus()
        }
    }
    
    func downloadForecastData(completed: @escaping DownloadComplete) {
        weatherApi = WeatherAPI(location: "Kiev", requestType: .Forecast16Days)
        
        guard let requestedURL = URL(string: weatherApi.requestedURL) else { return }
        
        Alamofire.request(requestedURL).responseJSON { response in
            let value = response.value as? Dictionary<String, AnyObject>
            guard let list = value?["list"] as? [Dictionary<String, AnyObject>] else { return }
            for obj in list {
                let forecast = Forecast(weatherDict: obj)
                self.forecasts.append(forecast)
            }
            self.forecasts.remove(at: 0)
            // end of code
            completed()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if forecasts.count > 0 {
            return forecasts.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherCell {
            if forecasts.count > 0 {
                cell.updateCell(forecast: forecasts[indexPath.row])
            }
            return cell
        } else {
            return WeatherCell()
        }
    }
    
    func updateMainUI() {
        dateLabel.text     = currentWeather.date
        tempLabel.text     = String(currentWeather.currentTemp) + "°"
        locationLabel.text = currentWeather.cityName
        weaterLabel.text   = currentWeather.weatherType
        imageLabel.image   = UIImage(named: currentWeather.weatherType)
    }
    
}

